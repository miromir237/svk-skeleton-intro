#!/bin/sh

# Create namespace
kubectl create namespace skeleton

# Create registry secret
# BEWARE: This puts link to osxkeychain on Mac OSX.
kubectl create secret docker-registry registry-cred --from-file=.dockerconfigjson=$HOME/.docker/config.json -n skeleton

# Create default-headers middleware
kubectl apply -f default-headers.yaml

# Create deployment
kubectl apply -f deployment.yaml

# Create service
kubectl apply -f service.yaml

# Create ingress
kubectl apply -f ingress.yaml

