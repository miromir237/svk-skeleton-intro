#!/bin/sh

# Delete ingress
kubectl delete -f ingress.yaml

# Delete service
kubectl delete -f service.yaml

# Delete deployment
kubectl delete -f deployment.yaml

# Delete default-headers middleware
kubectl delete -f default-headers.yaml

# Delete registry secret
kubectl delete secret registry-cred -n skeleton

# Delete namespace
kubectl delete namespace skeleton

