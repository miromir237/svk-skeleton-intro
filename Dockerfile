# Dockerfile to create Nodejs container image
# Based on nodejs:slim

FROM node:slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build
CMD node build/index.js
EXPOSE 3000
